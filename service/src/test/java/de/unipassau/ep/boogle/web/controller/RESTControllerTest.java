package de.unipassau.ep.boogle.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.unipassau.ep.boogle.persistence.entities.PrefixEntity;
import de.unipassau.ep.boogle.persistence.entities.QueryEntity;
import de.unipassau.ep.boogle.persistence.repositories.PrefixRepository;
import de.unipassau.ep.boogle.persistence.repositories.QueryRepository;
import de.unipassau.ep.boogle.web.dto.QueryDTO;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class RESTControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private QueryRepository queryRepository;
    @MockBean
    private PrefixRepository prefixRepository;

    @BeforeEach
    void mockQueryRepo() {
        QueryEntity savedQuery = new QueryEntity("1", "1", false, "test", new Date());
        List<QueryEntity> queries = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            queries.add(savedQuery);
        }

        Mockito.when(queryRepository.findAll(PageRequest.of(0, 50)))
                .thenReturn(new PageImpl<QueryEntity>(queries));
        Mockito.when(queryRepository.findAll(PageRequest.of(0, 5)))
                .thenReturn(new PageImpl<QueryEntity>(queries.subList(0, 5)));
        Mockito.when(queryRepository.findByUserId("1"))
                .thenReturn(new ArrayList<QueryEntity>(queries.subList(0, 5)));
        Mockito.when(queryRepository.save(any(QueryEntity.class)))
                .thenReturn(savedQuery);
        Mockito.when(queryRepository.existsById("1"))
                .thenReturn(true);
    }

    @BeforeEach
    void mockPrefixRepo() {
        PrefixEntity savedPrefix = new PrefixEntity("test", "uri", 1000);
        List<PrefixEntity> prefixes = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            prefixes.add(savedPrefix);
        }

        Mockito.when(prefixRepository.findByOrderByCountDesc(PageRequest.of(0, 1000)))
                .thenReturn(new PageImpl<PrefixEntity>(prefixes));
        Mockito.when(prefixRepository.findByOrderByCountDesc(PageRequest.of(0, 5)))
                .thenReturn(new PageImpl<PrefixEntity>(prefixes.subList(0, 5)));
    }

    @Test
    void ping() throws Exception {
        this.mockMvc.perform(post("/api/ping")
                .content("test"))
                .andExpect(status().isOk())
                .andExpect(content().string("test *** SUCCESS ***"));
    }

    @Nested
    class Sparql {
        String validRequest = "select distinct ?a ?b ?c where { VALUES (?a ?b ?c) { (\"a\" \"b\" \"c\") } } LIMIT 1";
        String invalidRequest = "select distinct ?a ?b ?c wo { VALUES (?a ?b ?c) { (\"a\" \"b\" \"c\") } } LIMIT 1";

        @Test
        void validQuery() throws Exception {
            runQuery(validRequest, true);
        }

        @Test
        void invalidQuery() throws Exception {
            runQuery(invalidRequest, false);
        }

        private void runQuery(String request, boolean isValid) throws Exception {
            ObjectNode jsonNode = (new ObjectMapper()).createObjectNode();
            jsonNode.put("request", request);
            jsonNode.put("endpoint", "WIKIDATA");

            if (isValid) {
                RESTControllerTest.this.mockMvc.perform(post("/api/sparql")
                        .content(jsonNode.toString()))
                        .andExpect(status().isOk())
                        .andExpect(content().string("[{\"a\":\"a\",\"b\":\"b\",\"c\":\"c\"}]"));
            } else {
                RESTControllerTest.this.mockMvc.perform(post("/api/sparql")
                        .content(jsonNode.toString()))
                        .andExpect(status().isBadRequest())
                        .andExpect(jsonPath("$.detail.line", is("1")));
            }
        }
    }

    @Nested
    class FullText {
        @Test
        void minimumParameters() throws Exception {
            RESTControllerTest.this.mockMvc.perform(post("/api/fulltext")
                    .param("q", "auto"))
                    .andExpect(status().isOk())
                    .andExpect(content().string(Matchers.not(Matchers.emptyString())));
        }

        @Test
        void maximumParameters() throws Exception {
            RESTControllerTest.this.mockMvc.perform(post("/api/fulltext")
                    .param("q", "auto")
                    .param("language", "en")
                    .param("page", "0")
                    .param("limit", "1")
                    .param("endpoint", "WIKIDATA"))
                    .andExpect(status().isOk())
                    .andExpect(content().string(Matchers.not(Matchers.emptyString())));
        }
    }

    @Nested
    class Elastic {
        @Test
        void minimumParameters() throws Exception {
            RESTControllerTest.this.mockMvc.perform(get("/api/elastic")
                    .param("q", "auto"))
                    .andExpect(status().isOk())
                    .andExpect(content().string(Matchers.not(Matchers.emptyString())));
        }

        @Test
        void maximumParameters() throws Exception {
            RESTControllerTest.this.mockMvc.perform(get("/api/elastic")
                    .param("q", "auto")
                    .param("language", "en")
                    .param("limit", "1")
                    .param("endpoint", "WIKIDATA"))
                    .andExpect(status().isOk())
                    .andExpect(content().string(Matchers.not(Matchers.emptyString())));
        }
    }

    @Nested
    class GetAllQueries {
        @Test
        void getAllQueries() throws Exception {
            RESTControllerTest.this.mockMvc.perform(get("/api/queries")
                    .param("page", "0"))
                    .andExpect(status().isOk())
                    .andExpect(content().string(Matchers.not(Matchers.emptyString())));
        }

        @Test
        void getFiveQueries() throws Exception {
            RESTControllerTest.this.mockMvc.perform(get("/api/queries")
                    .param("page", "0")
                    .param("size", "5"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(5)));
        }
    }

    @Test
    void getUserQueries() throws Exception {
        this.mockMvc.perform(get("/api/queries/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    void saveQuery() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        QueryDTO queryDTO = new QueryDTO(null, "1", null, false, null, null);

        this.mockMvc.perform(post("/api/queries/save")
                .content(mapper.writeValueAsString(queryDTO))
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId", is("1")));
    }

    @Test
    void updateQuery() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        QueryDTO queryDTO = new QueryDTO("1", "1", null, false, null, null);

        this.mockMvc.perform(put("/api/queries/1")
                .contentType("application/json")
                .content(mapper.writeValueAsString(queryDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId", is("1")));
    }

    @Test
    void getPredicates() throws Exception {
        String entity = "[\"http://www.wikidata.org/entity/Q2013\"]";

        this.mockMvc.perform(post("/api/query/predicates")
                .contentType("application/json")
                .content(entity).param("endpoint", "WIKIDATA"))
                .andDo((result) -> System.out.println(result.getResponse().getContentAsString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.not(Matchers.empty())));
    }

    @Test
    void deleteQuery() throws Exception {
        this.mockMvc.perform(delete("/api/queries/1/delete"))
                .andExpect(status().isOk())
                .andExpect(content().string("1"));
    }

    @Test
    void deleteUserQueries() throws Exception {
        this.mockMvc.perform(delete("/api/queries/delete/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("1"));
    }

    @Nested
    class Prefixes {
        @Test
        void getAllPrefixes() throws Exception {
            RESTControllerTest.this.mockMvc.perform(get("/api/prefixes"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(50)));
        }

        @Test
        void getFivePrefixes() throws Exception {
            RESTControllerTest.this.mockMvc.perform(get("/api/prefixes")
                    .param("page", "0")
                    .param("size", "5"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(5)));
        }
    }
 }