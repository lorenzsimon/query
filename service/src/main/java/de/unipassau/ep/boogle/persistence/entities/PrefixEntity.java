package de.unipassau.ep.boogle.persistence.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * This class represents prefix objects that are stored in the mongo db.
 */
@Data
@Document(collection = "Prefixes")
public class PrefixEntity {

    @Id
    private String id;
    private String uri;
    private String name;
    private long count;

    /**
     * Constructs a new PrefixEntity object.
     *
     * @param name The name of the prefix (e. g. "wikibase").
     * @param uri The uri of the prefix (e. g. "http://www.wikibase.org/").
     * @param count The number of uses.
     */
    public PrefixEntity(String name, String uri, long count) {
        this.name = name;
        this.uri = uri;
        this.count = count;
    }

    /**
     * Increments to number of uses of the prefix by one.
     *
     * @return The updated number of uses.
     */
    public long incrementCount() {
        return ++this.count;
    }
}
