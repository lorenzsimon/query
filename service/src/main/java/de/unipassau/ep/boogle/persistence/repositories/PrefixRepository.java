package de.unipassau.ep.boogle.persistence.repositories;

import de.unipassau.ep.boogle.persistence.entities.PrefixEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

/**
 * This repository manages prefix documents.
 */
public interface PrefixRepository extends MongoRepository<PrefixEntity, String> {
    Page<PrefixEntity> findByOrderByCountDesc(Pageable pageable);
    Optional<PrefixEntity> findByNameAndUri(String name, String uri);
}
