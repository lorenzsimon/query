package de.unipassau.ep.boogle.persistence.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * This class represents query objects that are stored in the mongo db.
 */
@Data
@NoArgsConstructor
@Document(collection = "Queries")
public class QueryEntity {

    @Id
    private String id;
    private String name;
    private String userId;
    private boolean isSPARQL;
    private String content;
    private Date date;

    /**
     * Constructs a new QueryEntity object.
     *
     * @param userId The id of the user that owns the query.
     * @param name The name of the query.
     * @param isSPARQL A flag that specifies if the query is a SPARQL query.
     * @param content The content of the query.
     * @param date The the creation date of the query.
     */
    public QueryEntity(String userId, String name, boolean isSPARQL, String content, Date date) {
        this.userId = userId;
        this.name = name;
        this.isSPARQL = isSPARQL;
        this.content = content;
        this.date = date;
    }

    /**
     * Constructs a new QueryEntity object for replacing existing queries.
     *
     * @param id The id of the query.
     * @param userId The id of the user that owns the query.
     * @param name The name of the query.
     * @param isSPARQL A flag that specifies if the query is a SPARQL query.
     * @param content The content of the query.
     * @param date The the creation date of the query.
     */
    public QueryEntity(String id, String name, String userId, boolean isSPARQL, String content, Date date) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.isSPARQL = isSPARQL;
        this.content = content;
        this.date = date;
    }
}
