package de.unipassau.ep.boogle.web.dto;

/**
 * Saves the Resource (URI) and the Literal (Label) of a SPARQL Query result
 * as {@link String}s.
 * It provides methods to receive the attribute values as well as a method
 * to return the information in a JSON formatted {@link String}.
 */
public class ResultSetDTO {
    private String uri;
    private String label;

    /**
     * Constructor that sets all attributes to the given values.
     * @param uri A {@link String} containing the sparql result resource.
     * @param label A {@link String} containing the sparql result literal.
     */
    public ResultSetDTO(String uri, String label) {
        this.uri = uri;
        this.label = label;
    }

    /**
     * Getter for the URI.
     * @return A {@link String} containing the resource / URI.
     */
    public String getUri() {
        return uri;
    }

    /**
     * Getter for the Label.
     * @return A {@link String} containing the literal / label.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Returns a String containing all data in JSON format.
     * @return A {@link String} in JSON format that contains the
     * URI as well as the Label.
     */
    @Override
    public String toString() {
        return "ResultSetEntity{\n"
                + "uri='" + uri + '\''
                + ",\nlabel='" + label + '\''
                + "\n}";
    }

    /**
     * {@inheritDoc}.
     *
     * Checks if the URI part of the {@link ResultSetDTO} contains the same
     * content.
     * @param o The {@link Object} to check.
     * @return {@code true} if and only if the objects the same or both
     * URIs contain the same content, otherwise {@code false}.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        final ResultSetDTO resultSetDTO = (ResultSetDTO) o;
        return this.uri.equals(resultSetDTO.uri);
    }

    /**
     * {@inheritDoc}.
     *
     * Returns the newly calculated hash code.
     * The URI is sufficient for the check for duplicates so only this is
     * used in the calculation.
     * @return The calculated hash code.
     */
    @Override
    public int hashCode() {
        final int prime = 31; // random prime number
        int result = 1;
        result = prime * result
                + ((this.uri == null) ? 0 : this.uri.hashCode());
        return result;
    }
}
