package de.unipassau.ep.boogle.persistence.repositories;

import de.unipassau.ep.boogle.persistence.entities.QueryEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This repository manages query documents.
 */
@Repository
public interface QueryRepository extends MongoRepository<QueryEntity, String> {
    List<QueryEntity> findByUserId(String UserId);
}
