package de.unipassau.ep.boogle.web.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This class represents a prefix for data transfer.
 */
@Data
@EqualsAndHashCode
public class PrefixDTO {
    private String uri;
    private String name;
    private long count;
    private String sparqlFormat;

    /**
     * Constructs a new PrefixDTO object.
     *
     * @param name The name of the prefix.
     * @param uri The uri of the prefix.
     * @param count The number of uses of the prefix.
     */
    public PrefixDTO(String name, String uri, long count) {
        this.name = name;
        this.uri = uri;
        this.count = count;
        this.sparqlFormat = "PREFIX " + name + ": " + "<" + uri + ">";
    }
}
