package de.unipassau.ep.boogle.application.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A IllegalParameterException indicates that a endpoint was called with an illegal parameter.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public final class IllegalParameterException extends IllegalArgumentException {

    /**
     * Creates an IllegalParameterException exception with the given message.
     *
     * @param msg String to create exception with
     */
    public IllegalParameterException(final String msg) {
        super(msg);
    }
}
