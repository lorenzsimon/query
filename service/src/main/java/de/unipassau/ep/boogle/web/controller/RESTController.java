package de.unipassau.ep.boogle.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hp.hpl.jena.query.QueryException;
import com.hp.hpl.jena.query.QueryParseException;
import com.hp.hpl.jena.sparql.algebra.Op;
import de.unipassau.ep.boogle.application.exceptions.IllegalParameterException;
import de.unipassau.ep.boogle.application.exceptions.NoSuchUserException;
import de.unipassau.ep.boogle.application.service.QueryService;
import de.unipassau.ep.boogle.web.dto.PrefixDTO;
import de.unipassau.ep.boogle.web.dto.ResultSetDTO;
import de.unipassau.ep.boogle.web.dto.ApiErrorDTO;
import de.unipassau.ep.boogle.web.dto.QueryDTO;
import de.unipassau.ep.logging.service.LoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * The class {@code RESTController} initializes the {@code QueryService} and
 * handles the URL mappings.
 */
@RestController
public class RESTController {
    private final QueryService queryService;
    private LoggingService logging;

    /**
     * The Constructor initializes the {@code QueryService} so it can be
     * used to process other requests sent via URL mappings.
     *
     * @param queryService The {@code QueryService} to be used by the
     *                     controller.
     * @param loggingService The {@code LoggingService} to be used by the
     *                       controller.
     */
    @Autowired
    public RESTController(QueryService queryService, LoggingService loggingService) {
        this.queryService = queryService;
        this.logging = loggingService;
    }

    /**
     * Handles a first communication test between the Frontend and the
     * Backend. It receives a {@code String} via POST and hands it over
     * to the {@code QueryService} in order to be further processed.
     * Then returns the resulting {@code String}.
     *
     * @param request A basic {@code String}.
     * @return The processed result as a {@code String}.
     */
    @PostMapping("/api/ping")
    public ResponseEntity<String> confirmRequest(@RequestBody String request) {
        logging.add("apiEndpoint","/api/ping").send("api");
        return ResponseEntity.ok(queryService.addSuccess(request));
    }

    /**
     * Handles the URI mapping for a SPARQL query.
     * Hands the query received via POST over to the {@code QueryService}
     * itself and returns the result.
     *
     * @param request A {@code String} that contains a SPARQL query.
     * @return The query result formatted as a {@code String}.
     * @throws JsonProcessingException If the request isn't valid JSON.
     */
    @PostMapping("/api/sparql")
    public ResponseEntity<String> executeQuery(@RequestBody String request) throws JsonProcessingException {
        // logging.add("apiEndpoint","/api/sparql").send("api");
        try {
            return ResponseEntity.ok(queryService.executeQuery(request).toString());
        } catch (IllegalArgumentException e) {
            throw new IllegalParameterException(e.getMessage());
        }
    }

    /**
     * Handles requests for fulltext queries.
     *
     * @param q The fulltext query.
     * @param language The language of the query.
     * @param page The page of the query result.
     * @param limit The limit of the query result.
     * @param endpoint The endpoint of the query.
     * @param predicates The additional predicates of the query.
     * @return The query result in JSON format.
     */
    @PostMapping("/api/fulltext")
    public ResponseEntity<String> executeFullTextQuery(@RequestParam String q, @RequestParam Optional<String> language, @RequestParam Optional<String> page, @RequestParam Optional<String> limit, @RequestParam Optional<String> endpoint, @RequestBody Optional<String> predicates) {
        logging.add("apiEndpoint","/api/fulltext").send("api");
        try {
            return ResponseEntity.ok(queryService.executeFullTextQuery(q, language.orElse("en"), page.orElse("0"), limit.orElse("100"), endpoint.orElse("WIKIDATA"), predicates.orElse("")).toString());
        } catch (NumberFormatException n) {
            throw new IllegalParameterException("Specified limit / page was not a integer");
        } catch (IllegalArgumentException e) {
            throw new IllegalParameterException(e.getMessage());
        }
    }

    /**
     * Handles requests for fulltext queries using a local elasticsearch instance.
     *
     * @param q The fulltext query
     * @return The query result in JSON format
     */
    @GetMapping("/api/elastic")
    public ResponseEntity<String> executeFullTextQuery(@RequestParam String q, @RequestParam Optional<String> language, @RequestParam Optional<String> limit, @RequestParam Optional<String> endpoint) {
        logging.add("apiEndpoint","/api/elastic").send("api");
        try {
            return ResponseEntity.ok(queryService.executeElasticQuery(q, language.orElse("en"), limit.orElse("100"), endpoint.orElse("WIKIDATA")).toString());
        } catch (NumberFormatException n) {
            throw new IllegalParameterException("Specified limit was not a integer");
        } catch (IllegalArgumentException e) {
            throw new IllegalParameterException(e.getMessage());
        }
    }

    /**
     * Handles requests for all queries in the database.
     *
     * @param page The page of the request.
     * @param size The size of the result page.
     * @return A List of queries of the specified page.
     */
    @GetMapping("/api/queries")
    public List<QueryDTO> getAllQueries(@RequestParam int page, @RequestParam Optional<Integer> size) {
        logging.add("apiEndpoint","/api/queries").send("api");
        return queryService.getAllQueries(page, size.orElse(50));
    }

    /**
     * Handles requests for queries of a specified user.
     *
     * @param userid The user whose queries are requested
     * @return A List of queries of the specified user
     */
    @GetMapping("/api/queries/{userid}")
    public List<QueryDTO> getUserQueries(@PathVariable String userid) {
        logging.add("apiEndpoint","/api/queries/{userid}").send("api");
        return queryService.getUserQueries(userid);
    }

    /**
     * Handles requests for saving a query in the database.
     *
     * @param query The query to be saved.
     * @return The Query that was saved in the database.
     */
    @PostMapping("/api/queries/save")
    public ResponseEntity<QueryDTO> saveQuery(@RequestBody QueryDTO query) {
        logging.add("apiEndpoint","/api/queries/save").send("api");
        return ResponseEntity.ok(queryService.saveQuery(query));
    }

    /**
     * Handles requests for updating an existing query in the database.
     *
     * @param query The query that replaces the query to be updated.
     * @param queryId The id of the query to be updated.
     * @return The query that was saved in the database.
     */
    @PutMapping("/api/queries/{queryId}")
    public QueryDTO updateQuery(@RequestBody QueryDTO query, @PathVariable String queryId) {
        logging.add("apiEndpoint", "/api/queries/save").send("api");
        return queryService.updateQuery(queryId, query);
    }

    /**
     * Returns all predicates of a given set of objects.
     *
     * @param object {@link List} of {@link String}s containing all objects
     *                           that have to be compared.
     * @return A {@link List} of {@link ResultSetDTO}s which contain the
     * Resource (URI) as well as the Literal (label) that the given objects
     * have.
     */
    @PostMapping("/api/query/predicates")
    public ResponseEntity<List<ResultSetDTO>> executePredicateSearch(@RequestBody List<String> object, @RequestParam String endpoint) {
        return new ResponseEntity<>(queryService.executePredicateSearch(object, endpoint), HttpStatus.OK);
    }

    /**
     * Calls the {@link QueryService} in order to delete all queries of the
     * given user.
     *
     * @param userid The user whose queries have to be deleted.
     * @return A {@link ResponseEntity} containing the [{@link HttpStatus} code
     * {@code HttpStatus.OK} if the user id was found and the queries have been
     * deleted, otherwise {@code HttpStatus.INTERNAL_SERVER_ERROR}.
     */
    @DeleteMapping("/api/queries/delete/{userid}")
    public ResponseEntity<String> deleteUserQueries(@PathVariable String userid) {
        queryService.deleteUserQueries(userid);
        return new ResponseEntity<>(userid, HttpStatus.OK);
    }

    /**
     * Handles requests for deleting a specific query from the database.
     *
     * @param queryid The id of the query to be deleted
     * @return The id of the deleted query.
     */
    @DeleteMapping("api/queries/{queryid}/delete")
    public String deleteQuery(@PathVariable String queryid) {
        return queryService.deleteQuery(queryid);
    }

    /**
     * Handles requests for prefixes in the database.
     *
     * @param page The page of the result.
     * @param size The size of the result page.
     * @return A List of prefixes of the specified page.
     */
    @GetMapping("/api/prefixes")
    public List<PrefixDTO> getAllPrefixes(@RequestParam Optional<Integer> page, @RequestParam Optional<Integer> size) {
        return queryService.getAllPrefixes(page.orElse(0), size.orElse(1000));
    }

    @ExceptionHandler(QueryParseException.class)
    private ResponseEntity<ApiErrorDTO> handleSyntaxError(HttpServletRequest request, QueryParseException e) {
        String message = QueryParseException.formatMessage(e.getMessage(), e.getLine(), e.getColumn());
        Map<String, String> detail = new HashMap<>();
        detail.put("line", Integer.toString(e.getLine()));
        detail.put("col", Integer.toString(e.getColumn()));
        return ResponseEntity.badRequest().body(new ApiErrorDTO(HttpStatus.BAD_REQUEST.value(), message, HttpStatus.BAD_REQUEST, request.getRequestURI(), detail));
    }
}
