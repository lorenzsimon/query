package de.unipassau.ep.boogle.application.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.sparql.core.Prologue;
import de.unipassau.ep.boogle.application.exceptions.NoSuchQueryException;
import de.unipassau.ep.boogle.application.exceptions.NoSuchUserException;
import de.unipassau.ep.boogle.persistence.entities.PrefixEntity;
import de.unipassau.ep.boogle.persistence.entities.QueryEntity;
import de.unipassau.ep.boogle.persistence.repositories.PrefixRepository;
import de.unipassau.ep.boogle.web.dto.PrefixDTO;
import de.unipassau.ep.boogle.web.dto.ResultSetDTO;
import de.unipassau.ep.boogle.persistence.repositories.QueryRepository;
import de.unipassau.ep.boogle.web.dto.QueryDTO;
import de.unipassau.ep.logging.service.LoggingService;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * The class {@code QueryService} functions as a bridge between the Frontend
 * and the WDQS API.
 * It receives either a Fulltext String or a defined SPARQL which is then
 * further processed to an actual {@code Query} and handed to the WDQS API.
 * The result is then processed on its own, saved in a database and returned
 * to the Frontend.
 */
@Service
public class QueryService {
    private final QueryRepository queryRepository;
    private final PrefixRepository prefixRepository;
    private final LoggingService logging;
    private final RestClient elastic;
    private final String wikiDataFullTextTemplate = readFullTextQueryTemplate(Endpoint.WIKIDATA);
    private final String virtuosoFullTextTemplate = readFullTextQueryTemplate(Endpoint.DBPEDIA);
    private final String wikiDataPredicatesTemplate = initPredicatesQueryString(Endpoint.WIKIDATA);
    private final String virtuosoPredicatesTemplate = initPredicatesQueryString(Endpoint.DBPEDIA);
    private final String PREDICATES_LITERAL = "wdLabel";
    private final String PREDICATES_RESOURCE = "wd";
    private String queryString;

    /**
     * Constructs a new QueryService object.
     *
     * @param queryRepository  The query repository to be used by the service
     * @param prefixRepository The prefix repository to be used by the service
     * @param loggingService   The logging service
     */
    @Autowired
    public QueryService(QueryRepository queryRepository, PrefixRepository prefixRepository, LoggingService loggingService, RestClient elastic) {
        this.queryRepository = queryRepository;
        this.prefixRepository = prefixRepository;
        this.logging = loggingService;
        this.elastic = elastic;
        initPredicatesQueryString();
    }

    /**
     * Initializes the Query String with the predicates SPARQL code.
     */
    private String initPredicatesQueryString(Endpoint endpoint) {
        // init the predicates query
        ClassPathResource resource;

        switch (endpoint) {
            case WIKIDATA:
                resource = new ClassPathResource("query_templates/wikidata_predicates.rq");
                break;
            case DBPEDIA:
            case BBC:
                resource = new ClassPathResource("query_templates/virtuoso_predicates.rq");
                break;
            default:
                throw new IllegalArgumentException("Unexpected value: " + endpoint);
        }

        try {
            byte[] data = FileCopyUtils.copyToByteArray(resource.getInputStream());
            return new String(data, StandardCharsets.UTF_8);
        } catch (IOException e) {
            return "";
        }
    }

    /**
     * The method functions a first communication test for the Frontend and the
     * Backend. It receives a {@code String}, appends a success message and
     * returns it as a new {@code String}.
     *
     * @param request The received message as a {@code String}.
     * @return A {@code String} containing the success message.
     */
    public String addSuccess(String request) {
        return request + " *** SUCCESS ***";
    }

    /**
     * The method transforms a received query {@code String} that contains
     * SPARQL code into an actual {@code Query} and performs it via the
     * WDQS API. The result is a {@code String} which is returned.
     *
     * @param request {@code String} containing the SPARQL query.
     * @return {@code String} that contains the search results.
     * @throws JsonProcessingException If the request isn't valid JSON.
     * @throws QueryException If the request isn't valid SPARQL.
     * @throws IllegalArgumentException If the endpoint isn't valid.
     */
    public JSONArray executeQuery(String request) throws JsonProcessingException, QueryException, IllegalArgumentException {
        logging.add("query", request).add("queryType", "sparQL").send("api");
        JsonNode parent = new ObjectMapper().readTree(request);
        String queryString = parent.path("request").asText();
        String endpoint = parent.path("endpoint").asText();

        Query query = QueryFactory.create(queryString);
        analysePrefixes(query);

        QueryExecution qexec;
        switch (endpoint.toUpperCase()) {
            case "":
            case "WIKIDATA":
                qexec = QueryExecutionFactory.createServiceRequest(Endpoint.WIKIDATA.getUrl(), query);
                break;
            case "DBPEDIA":
                qexec = QueryExecutionFactory.createServiceRequest(Endpoint.DBPEDIA.getUrl(), query);
                break;
            case "BBC":
                qexec = QueryExecutionFactory.createServiceRequest(Endpoint.BBC.getUrl(), query);
                break;
            default:
                throw new IllegalArgumentException("Unexpected value: " + endpoint.toUpperCase());
        }

        ResultSet results = qexec.execSelect();
        return resultSetToJSONArray(results);
    }

    /**
     * Executes a fulltext query that searches for items that have a label
     * containing the specified text.
     * The result contains the item, the items label and the items type.
     *
     * @param query    The query that is executed.
     * @param language The language of the query.
     * @param page The page of the result.
     * @param limit The requested limit of the query.
     * @param endpoint The chosen endpoint for the query.
     * @param predicates The additional predicates of the query.
     * @return The query result in JSON format.
     * @throws IllegalArgumentException If the given language isn't valid.
     */
    public JSONArray executeFullTextQuery(String query, String language, String page, String limit, String endpoint, String predicates) throws IllegalArgumentException {
        logging.add("query",query).add("language",language).add("page", page).add("limit",limit).add("endpoint",endpoint).add("queryType","fulltext").send("api");
        if (!language.toLowerCase().equals("en") && !language.toLowerCase().equals("de")) {
            throw new IllegalArgumentException("Unsupported language " + language.toLowerCase());
        }

        List<String> predicateList = null;
        if (predicates != null && predicates.length() > 0) {
            predicateList = constructPredicateList(predicates);
        }
        String predLabels = "";
        String predAddition = "";

        /* creates the sparql objects and the predicate additions.
         * object format:
         *      wikidata: ?objectLabel
         *      virtuoso: ?object
         * predicate addition format:
         *      wikidata: ?item wdt:predicate ?object
         *      virtuoso: ?item <predicate uri> ?object
         */
        if (predicateList != null && !predicateList.isEmpty()) {
            for (int i = 0; i < predicateList.size(); i++) {

                String object = predicateList.get(i).substring(predicateList.get(i).lastIndexOf("/") + 1);

                switch (endpoint.toUpperCase()) {
                    case "WIKIDATA":
                        predLabels += "?" + object + "Label ";
                        predAddition += "?item wdt:" + object + " ?" + object + ".\n";
                        break;
                    case "DBPEDIA":
                    case "BBC":
                        predLabels += "?" + object + " ";//"Label ";
                        predAddition += "?item <" + predicateList.get(i) + "> ?" + object + ".\n";
                        break;
                    default:
                        throw new IllegalArgumentException("Unexpected value: " + endpoint.toUpperCase());
                }
            }
        }

        Query jenaQuery;
        QueryExecution qexec;

        // Get the limit (1000 at most) an the offset (depends on limit and page)
        final String minLimit = String.valueOf(Math.min(Integer.parseInt(limit), 1000));
        final String offset = String.valueOf(Integer.parseInt(page) * Integer.parseInt(minLimit));

        String[] tokens = query.trim().split("\\s+");
        StringBuilder virtuosoQueryBuilder = new StringBuilder();
        for (int i = 0; i < tokens.length; i++) {
            if (i == (tokens.length - 1)) {
                virtuosoQueryBuilder.append("\'" + tokens[i] + "\'");
            } else {
                virtuosoQueryBuilder.append("\'" + tokens[i] + "\'" + " and ");
            }
        }


        switch (endpoint.toUpperCase()) {
        case "WIKIDATA":
            jenaQuery = QueryFactory.create(this.wikiDataFullTextTemplate
                    .replaceAll("_query_", query)
                    .replaceAll("_language_", language)
                    .replaceAll("_limit_", minLimit)
                    .replaceAll("_offset_", offset)
                    .replaceAll("_predicateLabel_", predLabels) // predicate Label in select part
                    .replaceAll("_predicate_", predAddition)); // predicate filtering

            qexec = QueryExecutionFactory.createServiceRequest(Endpoint.WIKIDATA.getUrl(), jenaQuery);
            break;
        case "DBPEDIA":
            jenaQuery = QueryFactory.create(this.virtuosoFullTextTemplate
                    .replaceAll("_query_", virtuosoQueryBuilder.toString())
                    .replaceAll("_language_", language)
                    .replaceAll("_offset_", offset)
                    .replaceAll("_limit_", minLimit)
                    .replaceAll("_limit_", minLimit)
                    .replaceAll("_predicateLabel_", predLabels)
                    .replaceAll("_predicate_", predAddition));

            qexec = QueryExecutionFactory.createServiceRequest(Endpoint.DBPEDIA.getUrl(), jenaQuery);
            break;
        case "BBC":
            jenaQuery = QueryFactory.create(this.virtuosoFullTextTemplate
                    .replaceAll("_query_", virtuosoQueryBuilder.toString())
                    .replaceAll("_language_", language)
                    .replaceAll("_limit_", minLimit)
                    .replaceAll("_predicateLabel_", predLabels)
                    .replaceAll("_predicate_", predAddition)
                    .replaceAll("_offset_", offset)
                    .replaceAll("_limit_", minLimit));

            qexec = QueryExecutionFactory.createServiceRequest(Endpoint.BBC.getUrl(), jenaQuery);
            break;
        default:
            throw new IllegalArgumentException("Unexpected value: " + endpoint.toUpperCase());
        }

        qexec.setTimeout(10000, 60000);
        ResultSet result = qexec.execSelect();
        return resultSetToJSONArray(result);
    }


    /**
     * Executes a fulltext query that uses the elasticsearch database for high performance requests.
     * The result contains the item, the items label and the items type.
     *
     * @param query    The query that is executed.
     * @param language The language of the query.
     * @param limit    The requested limit of the query.
     * @param endpoint The chosen endpoint for the query.
     * @return The query result in JSON format.
     */
    public JSONArray executeElasticQuery(String query, String language, String limit, String endpoint) throws IllegalArgumentException {
        logging.add("query", query).add("language", language).add("limit", limit).add("endpoint", endpoint).add("queryType", "elastic").send("api");
        if (!language.equalsIgnoreCase("en") && !language.equalsIgnoreCase("de")) {
            throw new IllegalArgumentException("Unsupported language " + language.toLowerCase());
        }
        if (elastic.isRunning()) {
            final Request request = new Request("GET", "/entities/_search");
            final String queryJson = "{\n" +
                    "  \"query\": {\n" +
                    "    \"wildcard\": {\n" +
                    "      \"labels\": {\n" +
                    "        \"value\": \"" + query + "*\",\n" +
                    "        \"boost\": 3.0,\n" +
                    "        \"rewrite\": \"constant_score\"\n" +
                    "      }\n" +
                    "    }\n" +
                    "  }\n" +
                    "}";
            request.setJsonEntity(queryJson.replace("\n", ""));
            JSONArray elasticResult = new JSONArray();
            try {
                Response res = elastic.performRequest(request);
                final byte[] bytes = res.getEntity().getContent().readAllBytes();
                final String resString = new String(bytes, StandardCharsets.UTF_8);
                final JSONObject resJson = new JSONObject(resString);
                final JSONArray ss = resJson.getJSONObject("hits").getJSONArray("hits");
                for (int i = 0; i < ss.length(); i++) {
                    JSONObject e = ss.getJSONObject(i).getJSONObject("_source");
                    elasticResult.put(new JSONObject("{" +
                            "\"item\": \"http://www.wikidata.org/entity/" + e.getString("id") + "\"," +
                            "\"typeLabel\": \"" + e.getString("labels") + "\"," +
                            "\"itemLabel\": \"" + e.getString("id") + "\"" +
                            "}"));
                }
                if (elasticResult.length() > 0) {
                    return elasticResult;
                }

            } catch (IOException e) {
                e.printStackTrace();
                
            }
        }
        return new JSONArray();
    }




    /**
     * Calculates the union of all predicates that the given objects have.
     * It then returns these predicates as a List.
     *
     * @param objects {@link List} of {@link String}s containing all objects.
     * @return {@link List} of {@link ResultSetDTO}s containing all predicates
     * of the given objects.
     */
    public List<ResultSetDTO> executePredicateSearch(List<String> objects, String endpoint) {
        Set<ResultSetDTO> predicates = new HashSet<>();
        String queryString = "";
        String url = "";

        switch (endpoint.toUpperCase()) {
            case "WIKIDATA":
                queryString = wikiDataPredicatesTemplate;
                url = Endpoint.WIKIDATA.getUrl();
                break;
            case "DBPEDIA":
                queryString = virtuosoPredicatesTemplate;
                url = Endpoint.DBPEDIA.getUrl();
                break;
            case "BBC":
                queryString = virtuosoPredicatesTemplate;
                url = Endpoint.BBC.getUrl();
                break;
            default:
                throw new IllegalArgumentException("Unexpected value: " + endpoint.toUpperCase());
        }

        for (String object : objects) {
            Query query = QueryFactory.create(queryString.replaceAll("_object_", "<" + object + ">"));
            QueryExecution qexec = QueryExecutionFactory.createServiceRequest(url, query);
            ResultSet resultSet = qexec.execSelect();
            Set<ResultSetDTO> set = new HashSet<>();

            // iterated over the resultset and adds the resource and literal to the predicate set.
            while (resultSet.hasNext()) {
                QuerySolution querySolution = resultSet.next();
                String uri = querySolution.getResource(PREDICATES_RESOURCE).toString();
                String label = querySolution.getLiteral(PREDICATES_LITERAL).getString();
                ResultSetDTO resultSetDTO = new ResultSetDTO(uri, label);
                set.add(resultSetDTO);
            }

            // uses the overridden equals() and hashSet() methods from ResultSetDTO
            predicates.addAll(set);
        }

        return new ArrayList<>(predicates);
    }

    /**
     * Gets all queries that are stored in the database.
     *
     * @param page The page of the result.
     * @param size The size of the result page.
     * @return A List of queries of the specified page.
     */
    public List<QueryDTO> getAllQueries(int page, int size) {
        Page<QueryEntity> queryDTOPage = queryRepository.findAll(PageRequest.of(page, size));
        return queriesToDTOs(queryDTOPage.getContent());
    }

    /**
     * Gets all queries of a given user that are stored in the database.
     *
     * @param userid The user whose queries are being searched for.
     * @return The queries of the given user that are stored in the database.
     */
    public List<QueryDTO> getUserQueries(String userid) {
        return queriesToDTOs(queryRepository.findByUserId(userid));
    }

    /**
     * Deletes all queries for a given user by searching for all
     * {@link QueryEntity}s in the database that have a matching
     * {@code userid}.
     *
     * @param userid The user specified via the id
     * @return A {@link String} containing "Success." after the deletion.
     * @throws NoSuchUserException If the user id was not found.
     */
    public String deleteUserQueries(String userid) throws NoSuchUserException {
        List<QueryEntity> userQueries = queryRepository.findByUserId(userid);

        if (userQueries.isEmpty()) {
            throw new NoSuchUserException("The given user does not exist");
        }

        for (QueryEntity queryEntity : userQueries) {
            queryRepository.delete(queryEntity);
        }

        return "Success.";
    }

    /**
     * Deletes the query with the given id from the database.
     *
     * @param queryid The id of the query to be deleted.
     * @return The id of the deleted query.
     */
    public String deleteQuery(String queryid) {
        if (!queryRepository.existsById(queryid)) {
            throw new NoSuchQueryException("The given query does not exist");
        }

        queryRepository.deleteById(queryid);
        return queryid;
    }

    /**
     * Saves a given query in the database.
     *
     * @param query The query to be saved.
     * @return The query that was saved in the database.
     * @throws NoSuchUserException If the specified user does not exist.
     */
    public QueryDTO saveQuery(QueryDTO query) throws NoSuchUserException {
        if (!queryIsValid(query)) throw new NoSuchUserException("The given user does not exist");
        return queryToDTO(queryRepository.save(new QueryEntity(query.getUserId(), query.getName(), query.isSPARQL(), query.getContent(), new Date())));
    }

    /**
     * Updates an existing query in the database.
     *
     * @param queryId The id of the query to be updated.
     * @param query The query that replaces the query to be updated.
     * @return The query that was saved in the database.
     */
    public QueryDTO updateQuery(String queryId, QueryDTO query) {
        if (!queryRepository.existsById(queryId)) {
            throw new NoSuchQueryException("The given query does not exist");
        }

        return queryToDTO(queryRepository.save(new QueryEntity(queryId, query.getName(), query.getUserId(), query.isSPARQL(), query.getContent(), query.getDate() != null ? query.getDate() : new Date())));
    }

    /**
     * Gets all prefixes that are stored in the database, ordered by the number of uses.
     *
     * @param page The page of the results.
     * @param size The size of the result pages.
     * @return A List of prefixes of the specified page, ordered by the number of uses.
     */
    public List<PrefixDTO> getAllPrefixes(int page, int size) {
        Page<PrefixEntity> prefixEntityPage = prefixRepository.findByOrderByCountDesc(PageRequest.of(page, size));
        return prefixesToDTOs(prefixEntityPage.getContent());
    }

    private void analysePrefixes(Query query) {
        Prologue prologue = query.getPrologue();
        Map<String, String> prefixes = prologue.getPrefixMapping().getNsPrefixMap();

        for (Map.Entry<String, String> pair : prefixes.entrySet()) {
            Optional<PrefixEntity> prefixEntity = prefixRepository.findByNameAndUri(pair.getKey(), pair.getValue());

            if (prefixEntity.isPresent()) {
                PrefixEntity entity = prefixEntity.get();
                entity.incrementCount();
                prefixRepository.save(entity);
            } else {
                prefixRepository.save(new PrefixEntity(pair.getKey(), pair.getValue(), 1));
            }
        }
    }

    private List<PrefixDTO> prefixesToDTOs(List<PrefixEntity> prefixes) {
        List<PrefixDTO> prefixDTOs = new ArrayList<>();

        for (PrefixEntity prefix : prefixes) {
            prefixDTOs.add(prefixToDTO(prefix));
        }

        return prefixDTOs;
    }

    private PrefixDTO prefixToDTO(PrefixEntity prefix) {
        return new PrefixDTO(prefix.getName(), prefix.getUri(), prefix.getCount());
    }

    private String readFullTextQueryTemplate(Endpoint endpoint) {
        ClassPathResource resource;

        switch (endpoint) {
            case WIKIDATA:
                resource = new ClassPathResource("query_templates/wikidata_fulltext.rq");
                break;
            case DBPEDIA:
                resource = new ClassPathResource("query_templates/virtuoso_fulltext.rq");
                break;
            default:
                throw new IllegalArgumentException("Unexpected value: " + endpoint);
        }

        try {
            byte[] data = FileCopyUtils.copyToByteArray(resource.getInputStream());
            return new String(data, StandardCharsets.UTF_8);
        } catch (IOException e) {
            return "";
        }
    }

    private JSONArray resultSetToJSONArray(ResultSet result) {
        JSONArray jsonArray = new JSONArray();
        List<String> colNames = result.getResultVars();

        while (result.hasNext()) {
            QuerySolution solution = result.next();
            JSONObject jsonObject = new JSONObject();

            for (String colName : colNames) {
                if (solution.get(colName) == null) {
                    jsonObject.put(colName, "null");
                    continue;
                }

                //System.out.println("colName: " + colName);
                if (solution.get(colName).isLiteral()) {

                    //System.out.println("Literal: " + solution.getLiteral(colName).toString());
                    jsonObject.put(colName, solution.getLiteral(colName).getString());
                } else if (solution.get(colName).isResource()) {

                    //System.out.println("Resource: " + solution.getResource(colName).toString());
                    jsonObject.put(colName, solution.getResource(colName).getURI());//.toString());
                }
            }
            jsonArray.put(jsonObject);
        }

        return jsonArray;
    }

    private QueryDTO queryToDTO(QueryEntity query) {
        return new QueryDTO(query.getId(), query.getUserId(), query.getName(), query.isSPARQL(), query.getContent(), query.getDate());
    }

    private List<QueryDTO> queriesToDTOs(List<QueryEntity> queries) {
        List<QueryDTO> queryDTOs = new ArrayList<>();

        for (QueryEntity query : queries) {
            queryDTOs.add(queryToDTO(query));
        }

        return queryDTOs;
    }

    private boolean queryIsValid(QueryDTO query) {
        return query.getUserId() != null;
    }

    private List<String> constructPredicateList(String predicates) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        List<String> predicateList = null;
        try {
            predicateList = mapper.readValue(predicates, List.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return predicateList;
    }

    private void initPredicatesQueryString() {

        // init the predicates query
        ClassPathResource resource = new ClassPathResource("query_templates/wikidata_predicates.rq");
        try {
            byte[] data = FileCopyUtils.copyToByteArray(resource.getInputStream());
            queryString = new String(data, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
