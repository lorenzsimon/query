package de.unipassau.ep.boogle.web.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * This class represents a query for data transfer.
 */
@Data
@EqualsAndHashCode
public class QueryDTO {
    private String id;
    private String userId;
    private String name;
    private boolean isSPARQL;
    private String content;
    private Date date;


    /**
     * Constructs a new QueryDTO object.
     *
     * @param id The id of the query.
     * @param userId The user id of the query owner.
     * @param name The name of the query.
     * @param isSPARQL Flag if the query is in SPARQL format.
     * @param content The content of the query.
     * @param date The creation date of the query.
     */
    public QueryDTO(String id, String userId, String name, boolean isSPARQL, String content, Date date) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.isSPARQL = isSPARQL;
        this.content = content;
        this.date = date;
    }
}
