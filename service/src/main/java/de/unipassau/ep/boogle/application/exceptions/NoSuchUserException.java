package de.unipassau.ep.boogle.application.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A NoSuchUser Exception indicates, that a user was specified that does not exist.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public final class NoSuchUserException extends RuntimeException {

    /**
     * Creates a NoSuchUser exception with the given message.
     *
     * @param msg String to create exception with
     */
    public NoSuchUserException(final String msg) {
        super(msg);
    }
}
