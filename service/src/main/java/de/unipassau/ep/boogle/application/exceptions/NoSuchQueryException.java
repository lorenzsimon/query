package de.unipassau.ep.boogle.application.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A NoSuchQuery Exception indicates, that a query was specified that does not exist.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public final class NoSuchQueryException extends RuntimeException {

    /**
     * Creates a NoSuchQuery exception with the given message.
     *
     * @param msg String to create exception with
     */
    public NoSuchQueryException(final String msg) {
        super(msg);
    }
}