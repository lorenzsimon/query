package de.unipassau.ep.boogle;

import de.unipassau.ep.logging.service.LoggingService;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

/**
 * The class provides the main method and is used to configure the application.
 */
@SpringBootApplication
@EnableMongoRepositories
@EnableEurekaClient
public class BoogleApplication {

	/**
	 * Represent the start point of the application.
	 *
	 * @param args Arguments that are passed at the start.
	 */
	public static void main(String[] args) {
		SpringApplication.run(BoogleApplication.class, args);
	}

	/**
	 * Declares the {@code RestTemplate} bean.
	 *
	 * @param builder The {@code RestTemplateBuilder} to be used.
	 * @return The {@code RestTemplate} managed by spring.
	 */
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	/**
	 * Declares the {@code LoggingService} bean.
	 *
	 * @return The {@code LoggingService} managed by spring.
	 */
	@Bean
	@Scope("singleton")
	public LoggingService loggingService() {
		return new LoggingService("query");
	}

	/**
	 * Declares the {@code WebClient.Builder} bean.
	 *
	 * @return The load balanced {@code WebClient.Builder} managed by spring.
	 */
	@Bean
	@LoadBalanced
	public WebClient.Builder webClient() {
		return WebClient.builder();
	}

	/**
	 * Creates a elasticsearch connection bean.
	 * 
	 * @return the build HttpHost to ElasticsSearchs rest-API
	 */
	@Bean
    @Scope("singleton")
    public RestClient elastic() {
        final CredentialsProvider credentialsProvider =
                new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials("elastic", "iloveboogle"));
        return RestClient.builder(
                new HttpHost("db-elastic", 9200))
                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                    @Override
                    public HttpAsyncClientBuilder customizeHttpClient(
                            HttpAsyncClientBuilder httpClientBuilder) {
                        return httpClientBuilder
                                .setDefaultCredentialsProvider(credentialsProvider);
                    }
                }).build();
    }
}
