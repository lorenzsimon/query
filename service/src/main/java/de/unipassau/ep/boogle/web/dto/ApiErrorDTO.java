package de.unipassau.ep.boogle.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represents an api error for data transfer.
 */
@Data
@EqualsAndHashCode
public class ApiErrorDTO {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private int status;
    private HttpStatus error;
    private String message;
    private String path;
    private Map<String, String> detail;

    /**
     * Constructs a new PrefixDTO object.
     *
     * @param status The http status code of the error.
     * @param message The message of the error.
     * @param error The string representation of the http error.
     * @param path The path where the error occurred.
     * @param detail Details of the error.
     */
    public ApiErrorDTO(int status, String message, HttpStatus error, String path, Map<String, String> detail) {
        this.timestamp = LocalDateTime.now();
        this.status = status;
        this.message = message;
        this.error = error;
        this.path = path;
        this.detail = new HashMap<>(detail);
    }
}
