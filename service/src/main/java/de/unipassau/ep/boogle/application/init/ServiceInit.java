package de.unipassau.ep.boogle.application.init;

import de.unipassau.ep.logging.service.LoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * This class initialises the service at start-up.
 */
@Component
public class ServiceInit implements CommandLineRunner {
    private final LoggingService logger;

    /**
     * Creates a new ServiceInit object.
     *
     * @param logger The logger to be initialised.
     */
    @Autowired
    public ServiceInit(LoggingService logger) {
        this.logger = logger;
    }

    /**
     * Method that is executed when the application has been started
     * (all spring components are available).
     *
     * @param args Arguments that are passed at the start.
     */
    @Override
    public void run(String... args) {
        initLogger();
    }

    private void initLogger() {
        logger.registerFields(new String[]{"apiEndpoint"});
        logger.registerTags(new String[]{"query","language","limit","endpoint"});
        logger.registerFields(new String[]{"queryType"});
    }
}
