package de.unipassau.ep.boogle.application.service;

/**
 * The Endpoint enum represents the supported SPARQL endpoints.
 */
public enum Endpoint {
    WIKIDATA ("https://query.wikidata.org/sparql"),
    DBPEDIA ("https://dbpedia.org/sparql"),
    BBC ("http://lod.openlinksw.com/sparql");

    private final String url;

    private Endpoint(String url) {
        this.url = url;
    }

    /**
     * Gets the URL of the endpoint.
     *
     * @return The URL of the endpoint.
     */
    public String getUrl() {
        return this.url;
    }
}
