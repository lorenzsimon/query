package de.unipassau.ep.boogle.application.init;

import de.unipassau.ep.boogle.persistence.entities.PrefixEntity;
import de.unipassau.ep.boogle.persistence.entities.QueryEntity;
import de.unipassau.ep.boogle.persistence.repositories.PrefixRepository;
import de.unipassau.ep.boogle.persistence.repositories.QueryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * This class initialises the database with values at start-up.
 */
@Component
public class DataInit implements CommandLineRunner {

    private final boolean init;
    private final QueryRepository queryRepository;
    private final PrefixRepository prefixRepository;
    private final Logger localLogger = LoggerFactory.getLogger(DataInit.class);

    /**
     * Constructs a new DataInit object.
     *
     * @param queryRepository The query repository of the database to be initialised.
     * @param prefixRepository The prefix repository of the database to be initialised.
     * @param init A flag that signals whether the init option was selected at start-up.
     */
    @Autowired
    public DataInit(QueryRepository queryRepository, PrefixRepository prefixRepository, @Value("${database.init}") boolean init) {
        this.queryRepository = queryRepository;
        this.prefixRepository = prefixRepository;
        this.init = init;
    }

    /**
     * Method that is executed when the application has been started
     * (all spring components are available).
     *
     * @param args Arguments that are passed at the start.
     * @throws Exception Any exception that was thrown in the process.
     */
    @Override
    public void run(String... args) throws Exception {
        if (init) {
            try {
                initQueries();
                initPrefixes();
            } catch (DataAccessResourceFailureException e) {
                localLogger.error(e.getMessage());
            }
        }
    }

    private void initQueries() {
        queryRepository.deleteAll();

        int i = 1;

        // Add 15 queries to the database
        for (; i <= 15; i++) {

            // The first 5 queries are not SPARQL queries, the last 10 queries are SPARQL queries
            if (i <= 5) {
                queryRepository.save(new QueryEntity("_TEST-USER-1_", "_QUERY" + "-NAME-" + i + "_", false, "_QUERY-CONTENT-" + i + "_", new Date()));
            } else {
                queryRepository.save(new QueryEntity("_TEST-USER-1_", "_QUERY" + "-NAME-" + i + "_", true, "_QUERY-CONTENT-" + i + "_", new Date()));
            }
        }

        queryRepository.save(new QueryEntity("_TEST-USER-2_", "_QUERY" + "-NAME-" + i + "_", false, "_QUERY-CONTENT-" + i + "_", new Date()));
        queryRepository.save(new QueryEntity("_TEST-USER-3_", "_QUERY" + "-NAME-" + ++i + "_", true, "_QUERY-CONTENT-" + i + "_", new Date()));
        queryRepository.save(new QueryEntity("_TEST-USER-4_", "_QUERY" + "-NAME-" + ++i + "_", false, "_QUERY-CONTENT-" + i + "_", new Date()));
        queryRepository.save(new QueryEntity("_TEST-USER-5_", "_QUERY" + "-NAME-" + ++i + "_", true, "_QUERY-CONTENT-" + i + "_", new Date()));
        queryRepository.save(new QueryEntity("_TEST-USER-6_", "_QUERY" + "-NAME-" + ++i + "_", false, "_QUERY-CONTENT-" + i + "_", new Date()));
    }

    private void initPrefixes() {
        prefixRepository.deleteAll();

        ClassPathResource resource = new ClassPathResource("query_templates/prefix_template.rq");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));

            // Save all prefixes of the file in the database
            while (reader.ready()) {
                String prefix = reader.readLine();
                prefixRepository.save(createPrefixEntity(prefix, 1000));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private PrefixEntity createPrefixEntity(String prefix, int count) {
        String name = prefix.substring(7, prefix.indexOf(":"));
        String uri = prefix.substring((prefix.indexOf("<")+1), prefix.indexOf(">"));

        return new PrefixEntity(name, uri, count);
    }
}
