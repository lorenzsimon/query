docker network create mongo-network
docker run --name mongodbopen --network mongo-network -v /data/db -p 27017:27017 --rm -d mongo:latest
docker run --name mondoexpress --network mongo-network -e ME_CONFIG_MONGODB_SERVER=mongodbopen -p 8088:8081 --rm -d mongo-express