FROM maven:3.6.3-jdk-11
COPY ./service .
RUN mvn package -Dmaven.test.skip=true

FROM openjdk:11
COPY --from=0 /target/*.jar .
CMD java -jar *.jar
