## Documentation of the Query-Service:


### What is the main purpose of the Query-Service?

The query service serves as a central consistent interface for making sparql and full text queries to the system. It acts as a proxy for various query endpoints and provides helpfull functionality for manipulating queries, such as adding additional predicates.

The service also manages stored user queries and prefixes that are used in queries. For each sparql query, the prefixes are analysed. They can be requested ordered by the number of uses.




### Internal Logic of the Query-Service:

The Query-Service consists of two parts, namely the Java Spring-Boot Application and the MongoDB Database.\
The service depends on the Discovery Service.
It has to be available in order to start the service and assure correct behaviour.

The Spring Application consists of the following parts:

 - QueryEntity Class:
    - Represents a request (SparQl or full-text), the id of the User making the request, as well as the content of the query.
 
 - PrefixEntity Class:
    - Represents a prefix (Name-URI pair) which is needed for SparQL queries as well as a usage counter.
        
 - QueryService Class:
    - Receives a query String, the endpoint, the predicates, the limit, offest and language from the frontend and processes it based on the search type.
        - SparQL: The query String consists of the prefixes and the query itself.
        It transforms the String to an actual Query and executes it based on the given endpoint.
        - Full-text: The query String consists of text which is added to a query template (based on the endpoint), as well as the other parameters.
        The String is then transformed into an actual query and then processed via the MediaWiki API.
        - Predicates: The query String consists of the URIs representing objects. These URIs are added to a template which is then processed and returns all predicates.
    - The result is returned as a JSON object containing Resource-Literal pairs.
    - Handles operations for saving, editing and deleting queries in the database.
    
 - QueryRepository Interface:
    - Manages CRUD operations for the saved queries.
    
 - PrefixRepository Interface:
    - Manages CRUD operations for the saved prefixes.

 - QueryController/ RESTController
    - Handles all the interfaces to outside Services that are requesting the results of a certain Query.
    
 - DataInit Class:
    - Initializes the prefix table with the main prefixes of wikidata.
    - Initializes the query table with dummy values.
    
 - ServiceInit:
    - Initializes the Logging Service that is used to log the called methods.
    
 - Exception Classes: IllegalParameterException, NoSuchUserException, NoSuchQueryException
    - The different exceptions handle possible errors while processing data.
    - Return HttpStatus codes including error messages.
    
 - DTO Classes: ApiErrorDTO, PrefixDTO, QueryDTO, ResultSetDTO
    - Handle the different return values of the service.
    
 The MongoDB database consists of 2 tables.
 - The query collection in the database consists of the following fields:
    - id (String)
    - userId (String)
    - name (String)
    - isSPARQL (boolean)
    - content (String)
    - date (Date)
    
 - The prefix collection in the database consists of the following fields:
    - id (String)
    - uri (String)
    - name (String)
    - count (long)


### Currently Available Endpoints of The Query-Service:

 **[POST] /api/ping**

- Purpose: Testing the accessibility of the query service.
- Receives a String via POST.
- Returns the query String plus a success message.

 **[POST] /api/sparql**

  - Purpose: Uses the received String to request a result from the supported SPARQL enpoints. 
  - Receives the SPARQL query `request` and the endpoint `endpoint` as JSON properties. Options for `endpoint` are `"WIKIDATA"`, `"DBPEDIA"` and `"BBC"`. If no endpoint is specified, the query is sent to WikiData by default.
  - Returns the query result objects in JSON format. 
  - In case of an incorrect query syntax, the endpoint returns a 400 (bad request) error. The response body contains the row `row` and column `col` of the error inside the `detail` field. 

 **[GET] /api/fulltext**

  - Purpose: Performing a full text query.

  - Receives the query term `q`, the language `language`, the limit `limit`, the page `page` and the endpoint `endpoint` as request parameters. Receives additional predicates `predicates` as request body. Options for `language` are `en` and `de`. Options for `endpoint` are `WIKIDATA`, `DBPEDIA` and `BBC`. Default `language` is `en`. Default `endpoint` is `WIKIDATA`. Default `limit` is `100`; maximum `limit` is `1000`. Default `page` is 0.

  - Returns `limit` query result objects in JSON format with a offset of `page * limit`. The object members are: `item` (item URL), `itemLabel` (content of the item label) and `typeLabel` (content of the item type).

 **[GET] /api/queries**

  - Purpose: Get all queries that are stored in the database.

  - Receives `page` and `size` as request parameters. Page specifies the result page (starting from `0`). Size specifies the size of the requested result page - 50 if not specified.

  - Returns a page of queries that are stored in the database.

 **[GET] /api/queries/{userId}**

  - Purpose: Get all queries of the user `userID` that are stored in the database.

  - Receives a userId as a pathvariable via GET.

  - Returns a list of all queries of the specified user that are stored in the database.

**[POST] /api/queries/save**

  - Purpose: Save a query in the database

  - Receives a query DTO object with a `userId`, `sparql` flag, a `name` and the query `content` via POST.
  
  - Returns the query that was saved in the database.

**[PUT] /api/queries/{queryId}**

  - Purpose: Update a query in the database

  - Receives a query DTO object with a `userId`, `sparql` flag, a `name`, a `date` and the query `content` via PUT. If no `date` is specified, the date of the query is updated to the current time.
  
  - Returns the query that was saved in the database.

**[DELETE] /api/queries/{queryid}/delete**

  - Purpose: Delete a given query in the database

  - Receives the query id as path variable
  
  - Returns the id of the deleted query with status `HttpStatus.OK` if the query was deleted. If the query does not exist it returns an error with status `HttpStatus.BAD_REQUEST`.

**[DELETE] /api/queries/delete/{userid}**
  - Purpose: Delete all queries that belong to a given user
  - Receives a simple String containing the user id via a PathVariable, searches for all queries that are saved from this user and deletes them.
  - Returns a ResponseEntity with the user id and `HttpStatus.OK` if the queries were deleted. If the user id was not found an Exception is thrown and therefore `HttpStatus.BAD_REQUEST` will be returned.

**[GET] /api/prefixes**

- Purpose: Get all prefixes that are stored in the database orderd by the number of uses (ascending)
- Receives `page`and `size`as request parameters. Page specifies the result page (starting from `0`). Size specifies the size of the requested result page. Default page is 0. Default size is 1000.
- Returns page `page` of the `size` most used prefixes that are stored in the database. 

**[POST] /api/query/predicates**

- Purpose: Get all predicates of a given set of objects.
- Receives a list of object URIs as request body.
- Returns a list of predicate objects with a `uri` and `label` field.



### Error Responses

In the case of errors, the API responds with `4**` or `5**` HTTP codes. The response body contains information about the time, type, message and details of the error. The detail field is optional. For specific error handling please refer to the endpoint documentation.

Example:

```json
{
  "timestamp": "2021-01-11 05:10:05",
  "status": 400,
  "error": "BAD_REQUEST",
  "message": "[line: 1, col: 24] Lexical error at line 1, column 30. Encountered: (32), after: 'wher'",
  "path": "/api/sparql",
  "detail": {
    "col": "24",
    "line": "1"
  }
}
```

### Known Bugs

- Addition of Predicates: When adding predicates to a full-text query template the predicate language cannot be set.
It has been tried in a simple version that just set the language but this version failed when the result contained numbers that did not have  a language tag.
It then returned an empty list instead of the values.
This can be changed in the future by adding an improved language switch that replaces a regex in the query template.